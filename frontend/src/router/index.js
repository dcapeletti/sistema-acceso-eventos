import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: () => import('@/views/Home.vue')
    },
    {
      path: '/about',
      name: 'About',
      component: () => import('@/views/About.vue')
    },
    {
      path: '*',
      name: 'NotFound',
      component: () => import('@/views/NotFount.vue')
    },
    {
      path: '/lecturas',
      name: 'Lecturas',
      component: () => import('@/views/Lecturas.vue')
    },
    {
      path: '/eventos',
      name: 'Eventos',
      component: () => import('@/views/Eventos.vue')
    },
    {
      path: '/credenciales',
      name: 'Credenciales',
      component: () => import('@/views/Credenciales.vue')
    },
    {
      path: '/molinetes',
      name: 'Molinetes',
      component: () => import('@/views/Molinetes.vue')
    }
  ]
})
