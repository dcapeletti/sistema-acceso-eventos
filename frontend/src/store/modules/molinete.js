/* eslint-disable */
import {
  serviceMolinete
} from '@/services/molinete.js'

export const molinete = {
  namespaced: true,
  state: {
    molinetes: []
  },
  getters: {

  },
  mutations: {
    rellenarMolinetes (state, molinetes) {
      state.molinetes = molinetes
    },
    agregarMolinete (state, molinete) {
      state.molinetes.push(molinete)
    },
    eliminarMolinete (state, molinete) {
      const index = state.molinetes.findIndex(item => item.id === molinete)
      state.molinetes.splice(index, 1)
    }
  },
  actions: {
    obtenerMolinetes ({commit}, {pagina, limite}) {
      serviceMolinete.listar(pagina, limite)
      .then((response) => {
        commit('rellenarMolinetes', response.data)
      })
      .catch((err) => {
        console.log(err)
      })
    },
    crearMolinete({commit}) {
      serviceMolinete.crear()
      .then((response) => {
        console.log(response.data[0])
        commit('agregarMolinete', response.data[0])
      })
      .catch((err) => {
        console.log(err)
      })
    },
    eliminarMolinete ({commit}, molinete) {
      serviceMolinete.eliminar(molinete)
      .then((response) => {
        alert(response.data.message)
        commit('eliminarMolinete', molinete)
        console.log(response)
      })
      .catch((err) => {
        alert(err.response.data.error)
        console.log(err)
      })
    }
  }
}
