/* eslint-disable */
import {
  serviceLectura
} from '@/services/lectura.js'

export const lectura = {
  namespaced: true,
  state: {
    lecturas: []
  },
  getters: {

  },
  mutations: {
    agregarNuevaLectura (state, lectura) {
      state.lecturas.push(lectura)
    },
    rellenarLecturas (state, lecturas) {
      state.lecturas = lecturas
    }
  },
  actions: {
    registrar ({commit, dispatch}, {evento, molinete, credencial}) {
      serviceLectura.registrar(evento, molinete, credencial)
      .then((response) => {
        console.log(response.data)
        commit('agregarNuevaLectura', response.data[0])
        alert('Registro correcto.')
      })
      .catch((error) => {
        console.log(error.response)
        alert('Error: ' + error.response.data.error)
      })
    },
    obtenerLecturas ({commit}, {pagina, limite}) {
      serviceLectura.listar(pagina, limite)
      .then((response) => {
        console.log('LECTURAS', response.data[0])
        commit('rellenarLecturas', response.data[0])
      })
      .catch((error) => {
        console.log("ERROR", error)
      })
    },
    obtenerLecturaEvento ({commit}, {evento, pagina, limite}) {
      serviceLectura.listarPorEvento(evento, pagina, limite)
      .then((response) => {
        commit('rellenarLecturas', response.data[0])
      })
      .catch((err) => {
        console.log(err)
        alert(err)
      })
    }
  }
}
