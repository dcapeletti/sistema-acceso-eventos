/* eslint-disable */
import {
  serviceEvento
} from '@/services/evento.js'

export const evento = {
  namespaced: true,
  state: {
    eventos: []
  },
  getters: {

  },
  mutations: {
    rellenarEventos(state, eventos) {
      state.eventos = eventos
    },
    agregarEvento(state, evento) {
      state.eventos.push(evento)
    },
    eliminarEvento(state, evento) {
      const index = state.eventos.findIndex(item => item.id === evento)
      state.eventos.splice(index, 1)
    }
  },
  actions: {
    listarEventos ({commit}, {pagina, limite}) {
      serviceEvento.listar(pagina, limite)
      .then((response) => {
        commit('rellenarEventos', response.data)
      })
      .catch((error) => {
        console.log(error)
      })
    },
    guardarEvento ({commit}, nombre) {
      serviceEvento.crear(nombre)
      .then((response) => {
        console.log(response.data[0])
        commit('agregarEvento', response.data[0])
      })
      .catch((err) => {
        console.log(err)
      })
    },
    eliminarEvento ({commit}, evento) {
      serviceEvento.eliminar(evento)
      .then((response) => {
        console.log(response.data)
        alert(response.data.message)
        commit('eliminarEvento', evento)
      })
      .catch((err) => {
        console.log(err)
        alert(err.response.data.error)
      })
    }
  }
}
