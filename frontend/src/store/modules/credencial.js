/* eslint-disable */
import {
  serviceCredencial
} from '@/services/credencial.js'

export const credencial = {
  namespaced: true,
  state: {
    credenciales: []
  },
  getters: {

  },
  mutations: {
    rellenarCredenciales (state, credenciales) {
      state.credenciales = credenciales
    },
    eliminarCredencial(state, credencial) {
      const index = state.credenciales.findIndex(item => item.id === credencial)
      state.credenciales.splice(index, 1)
    },
    agregarCredencial (state, credencial) {
      state.credenciales.push(credencial)
    }
  },
  actions: {
    obtenerCredenciales ({commit}, {pagina, limite}) {
      serviceCredencial.listar(pagina, limite)
      .then((response) => {
        console.log('CREDENCIALES', response.data)
        commit('rellenarCredenciales', response.data)
      })
      .catch((err) => {
        console.log(err)
      })
    },
    eliminarCredencial ({commit}, credencial) {
      serviceCredencial.eliminar(credencial)
      .then((response) => {
        console.log(response)
        alert(response.data.message)
        commit('eliminarCredencial', credencial)
      })
      .catch((err) => {
        alert(err.response.data.error)
        console.log(err.response.data)
      })
    },
    guardarCredencial ({commit}, propietarioCredencial) {
      serviceCredencial.guardar(propietarioCredencial)
      .then((response) => {
        commit('agregarCredencial', response.data[0])
        console.log(response)
      })
      .catch((err) => {
        console.log(err)
      })
    }
  }
}
