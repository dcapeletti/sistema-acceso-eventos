import Vue from 'vue'
import Vuex from 'vuex'
import {credencial} from './modules/credencial'
import {evento} from './modules/evento'
import {lectura} from './modules/lectura'
import {molinete} from './modules/molinete'
// import childA from './modules/granja'

// import {
//   serviceGranja
// } from '@/services/Granja.js'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    Credencial: credencial,
    Evento: evento,
    Lectura: lectura,
    Molinete: molinete
  }
})
