/* eslint-disable */
import axios from 'axios'

export const serverUrl = 'http://localhost:5000'

export const serviceCredencial = {
  listar: function (pagina, limite) {
    return axios.get(serverUrl + '/credencial?page=' + pagina + '&limit=' + limite)
  },
  contar: function () {
    return axios.get(serverUrl + '/credencial/contar')
  },
  guardar: function (propitario) {
    return axios.post(serverUrl + '/credencial', {
      'propietario': propitario
    })
  },
  eliminar: function (id) {
    return axios.delete(serverUrl + '/credencial/' + id)
  }
}
