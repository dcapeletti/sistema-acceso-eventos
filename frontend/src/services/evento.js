/* eslint-disable */
import axios from 'axios'

export const serverUrl = 'http://localhost:5000'

export const serviceEvento = {
  crear: function (nombre) {
    return axios.post(serverUrl + '/evento', {
      'nombre': nombre
    })
  },
  listar: function (pagina, limite) {
    return axios.get(serverUrl + '/evento?page=' + pagina + '&limit=' + limite)
  },
  eliminar: function (evento) {
    return axios.delete(serverUrl + '/evento/' + evento)
  },
  contar: function () {
    return axios.get(serverUrl + '/evento/contar')
  }
}
