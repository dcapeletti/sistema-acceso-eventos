/* eslint-disable */
import axios from 'axios'

export const serverUrl = 'http://localhost:5000'

export const serviceMolinete = {
  listar: function (pagina, limite) {
    return axios.get(serverUrl + '/molinete?page=' + pagina + '&limit=' + limite)
  },
  contar: function () {
    return axios.get(serverUrl + '/molinete/contar')
  },
  crear: function () {
    return axios.post(serverUrl + '/molinete')
  },
  eliminar: function (id) {
    return axios.delete(serverUrl + '/molinete/' + id)
  }
}
