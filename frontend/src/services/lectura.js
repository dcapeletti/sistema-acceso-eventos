/* eslint-disable */
import axios from 'axios'

export const serverUrl = 'http://localhost:5000'

export const serviceLectura = {
  registrar: function(evento, molinete, credencial) {
    return axios.post(serverUrl + '/lectura', {
      'evento': evento,
      'molinete': molinete,
      'credencial': credencial
    })
  },
  contar: function (evento) {
    return axios.get(serverUrl + '/lectura/count/' + evento)
  },
  total: function () {
    return axios.get(serverUrl + '/lectura/total')
  },
  listar: function (pagina, limite) {
    return axios.get(serverUrl + '/lectura?page=' + pagina + '&limit=' + limite)
  },
  listarPorEvento: function (evento, pagina, limite) {
    return axios.get(serverUrl + '/lectura/evento/' + evento + '?page=' + pagina + '&limit=' + limite)
  }
}
