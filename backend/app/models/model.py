from app import db, ma
import datetime

class Evento(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(80), unique=True, nullable=False)
    fechaCreacion = db.Column(db.DateTime, nullable=False, default=datetime.datetime.now)


class Molinete(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    # fechaCreacion = db.Column(db.DateTime, nullable=False)
    def leerCredencial(self, credencial):
        cred = Credencial.query.get(credencial)
        if cred != None: #Credencial existe
            return True
        else:
            return False


class Lectura(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    evento = db.Column(db.Integer, db.ForeignKey('evento.id'), nullable=False)
    evento_rel = db.relationship('Evento', backref='evento')
    molinete = db.Column(db.Integer, db.ForeignKey('molinete.id'), nullable=False)
    molinete_rel = db.relationship('Molinete', backref='molinete')
    credencial = db.Column(db.Integer, db.ForeignKey('credencial.id'), nullable=False)
    credencial_rel = db.relationship('Credencial', backref='crendencial')
    fecha = db.Column(db.DateTime, default=datetime.datetime.now, nullable=False)


class Credencial(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    propietario = db.Column(db.String(80), nullable=False) #Nombre del dueno de la credencial...


class EventoSchema(ma.ModelSchema):
    class Meta:
        model = Evento


class MolineteSchema(ma.ModelSchema):
    class Meta:
        model = Molinete


class CredencialSchema(ma.ModelSchema):
    class Meta:
        model = Credencial


class LecturaSchema(ma.ModelSchema):
    class Meta:
        model = Lectura
    evento_rel = ma.Nested(EventoSchema)
    credencial_rel = ma.Nested(CredencialSchema)



schema_evento = EventoSchema()
# Para devolucion de varios eventos...
schema_eventos = EventoSchema(many=True)

schema_molinete = MolineteSchema()
# Para devolucion de varios molinetes..
schema_molinetes = MolineteSchema(many=True)

schema_credencial = CredencialSchema()
# Para devolucion de varios molinetes..
schema_credenciales = CredencialSchema(many=True)

schema_lectura = LecturaSchema()
#Para devolucion de varias Lecturas
schema_lecturas = LecturaSchema(many=True)
