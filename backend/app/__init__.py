from flask import Flask
from flask_cors import CORS
import requests
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

app = Flask(__name__,
            static_folder="../dist/static",
            template_folder="../dist")

app.config.from_object("config.ProductionConfig")

cors = CORS(app, resources={r"/evento/*": {"origins": "*"},
                            r"/molinete/*": {"origins": "*"},
                            r"/credencial/*": {"origins": "*"},
                            r"/lectura/*": {"origins": "*"}})

# basedir = os.path.abspath(os.path.dirname(__file__))
# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'crud.sqlite')

db = SQLAlchemy(app)
ma = Marshmallow(app)


from app.routes import *
from app.routes.evento import *
from app.routes.molinete import *
from app.routes.credencial import *
from app.routes.lectura import *

if app.debug: #si esta en modo DevelopmentConfig importo las vistas del Admin...
    from app.views import *
    #http://http://localhost:5000/admin/
