from flask import request, jsonify, make_response
from app.models.model import Lectura, Evento, Molinete, Credencial, schema_lectura, schema_lecturas
from app import db, app

@app.route('/lectura/<int:id>', methods=['GET'])
def getLectura(id):
    lectura = Lectura.query.get(id)
    if lectura != None:
        return jsonify(schema_lectura.dump(lectura))
    else:
        return jsonify(error="La lectura no existe.")


@app.route('/lectura', methods=['GET'])
def getLecturas():
    pagina = request.args.get('page')
    limite = request.args.get('limit')
    if pagina != None and limite != None:
        lecturas = Lectura.query.paginate(int(pagina), int(limite), True)
        # print(schema_lecturas.dump(lecturas.items))
        return jsonify(schema_lecturas.dump(lecturas.items))
    else:
        lecturas = Lectura.query.paginate(1, Lectura.query.count(), True)
        return jsonify(schema_lecturas.dump(lecturas.items, many=True))


@app.route('/lectura/evento/<int:evento>', methods=['GET'])
def getLecturasEvento(evento):
    # curl -X GET -i 'http://localhost:5000/lectura/evento/1'
    pagina = request.args.get('page')
    limite = request.args.get('limit')
    if pagina != None and limite != None:
        lecturas = Lectura.query.filter_by(evento=evento).paginate(int(pagina), int(limite), True)
        # print(schema_lecturas.dump(lecturas.items))
        return jsonify(schema_lecturas.dump(lecturas.items))
    else:
        lecturas = Lectura.query.filter_by(evento=evento).paginate(1, Lectura.query.count(), True)
        return jsonify(schema_lecturas.dump(lecturas.items, many=True))


@app.route('/lectura/count/<int:evento>', methods=['GET'])
def contarLecturas(evento):
    # curl -X GET -i 'http://localhost:5000/lectura/count/1'
    evento = Evento.query.get(evento)
    print(type(evento))
    if evento != None:
        cantidadLecturas = Lectura.query.filter_by(evento=evento.id).count()
        return jsonify(CantidadLecturas=cantidadLecturas)
    else:
        return make_response(jsonify(error="El evento no existe."))


@app.route('/lectura/total')
def totalLecturas():
    # curl -X GET -i 'http://localhost:5000/lectura/total'
    total = Lectura.query.count()
    return jsonify(total=total)


'''
curl -X POST -H 'Content-Type: application/json' -i http://localhost:5000/lectura --data '{
"evento": "1",
"molinete": "1",
"credencial": 1
}'
'''
@app.route('/lectura', methods=['POST'])
def crearLectura():
    try:
        evento = request.json['evento']
        credencial = request.json['credencial']
        molinete = request.json['molinete']
        print("\nEvento", evento, "Molinete", molinete, "Credencial", credencial)
        if evento != '' or molinete != '' or credencial != '':
            evento = Evento.query.get(evento)
            molinete = Molinete.query.get(molinete)
            if evento != None and molinete != None:
                if molinete.leerCredencial(credencial):
                    lectura = Lectura(evento=evento.id, molinete=molinete.id, credencial=credencial)
                    db.session.add(lectura)
                    db.session.commit()
                    return jsonify(schema_lectura.dump(lectura))
                else:
                    return make_response(jsonify(error="La credencial no es valida."), 404)
            else:
                return make_response(jsonify(error="El evento o el molinete no existen."), 404)
        else:
            return make_response(jsonify(error="Datos invalidos o incompletos"), 404)
    except KeyError:
        return make_response(jsonify(error="Las claves de la solicitud son incorrectas."), 404)
