from app import app
from flask import render_template, make_response
import requests

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def capturar_todo(path):
    if app.debug:
        #Se esta en modo debug, se hace un proxy al servidor front-end
        return requests.get('http://localhost:8080/{}'.format(path)).text
    return render_template("index.html")
