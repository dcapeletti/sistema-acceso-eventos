from app import app, db
from flask import render_template, jsonify, make_response, request
from app.models.model import Credencial, schema_credencial, schema_credenciales
from sqlalchemy.exc import IntegrityError

@app.route('/credencial/<int:id>', methods=["GET"])
def getCredencial(id):
    # curl -X GET -i 'http://localhost:5000/credencial/1'
    credencial = Credencial.query.get(id)
    if credencial != None:
        result = schema_credencial.dump(credencial)
        return jsonify(result.data)
    else:
        return make_response(jsonify(error="La credencial no existe."), 404)


@app.route('/credencial', methods=["GET"])
def getCredenciales():
    pagina = request.args.get("page")
    limite = request.args.get("limit")
    print("pagina", pagina, "limite", limite)
    if pagina != None and limite != None: #curl -X GET -i 'http://localhost:5000/credencial?page=1&limit=1'
        credenciales = Credencial.query.paginate(int(pagina), int(limite), True)
        result = schema_credenciales.dump(credenciales.items, many=True)
        return jsonify(result.data)
    else: #Todas las credenciales curl -X GET -i 'http://localhost:5000/credencial'
        credenciales = Credencial.query.paginate(1, Credencial.query.count(), True)
        result = schema_credenciales.dump(credenciales.items, many=True)
        return jsonify(result.data)


@app.route('/credencial/contar', methods=['GET'])
def contarCredenciales():
    #curl -X GET -i 'http://localhost:5000/credencial/contar'
    cantidadTotal = Credencial.query.count()
    return jsonify(total = cantidadTotal)


'''
curl -X POST -H 'Content-Type: application/json' -i http://localhost:5000/credencial --data '{
"propietario": "Prueba"
}'
'''
@app.route('/credencial', methods=["POST"])
def crearCredencial():
    try:
        propietario = request.json['propietario']
        if propietario != "":
            credencial = Credencial(propietario=propietario)
            db.session.add(credencial)
            db.session.commit()
            return jsonify(schema_credencial.dump(credencial))
        else:
            return make_response(jsonify(error="Los datos ingresados son incorrectos."), 404)
    except KeyError:
        return jsonify(error="Los parametros son incorrectos.")


@app.route('/credencial/<int:id>', methods=['DELETE'])
def eliminarCredencial(id):
    try:
        credencial = Credencial.query.get(id)
        if credencial != None:
            db.session.delete(credencial)
            db.session.commit()
            return jsonify(message="Credencial eliminada correctamente.")
        else:
            return make_response(jsonify(error="La credencial no existe."), 404)
    except IntegrityError:
        return make_response(jsonify(error="La credencial no se puede eliminar porque tiene movimientos asociados."), 404)



'''
curl -X PUT -H 'Content-Type: application/json' -i http://localhost:5000/credencial/1 --data '{
"propitario": "Pruebaaaa"
}'
'''
@app.route('/credencial/<int:id>', methods=['PUT'])
def modificarCredencial(id):
    try:
        propietario = request.json['propietario']
        if propietario != "":
            credencial = Credencial.query.get(id)
            credencial.propietario = propietario
            db.session.add(credencial)
            db.session.commit()
            return jsonify(schema_credencial.dump(credencial))
        else:
            return make_response(jsonify(error="Datos incorrectos."))
    except KeyError:
        return make_response(jsonify(error="Los parametros son incorrectos."))
