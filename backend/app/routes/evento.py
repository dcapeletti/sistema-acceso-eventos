from app import app, db
from flask import render_template, jsonify, make_response, request
from app.models.model import Evento, schema_eventos, schema_evento
from sqlalchemy.exc import IntegrityError


@app.route('/evento/<int:id>', methods=['GET'])
def getEvento(id):
    evento = Evento.query.get(id)
    if evento != None:
        return jsonify(schema_evento.dump(evento))
    else:
        make_response(jsonify(error="El evento no existe."))


@app.route('/evento', methods=["GET"])
def getEventos():
    # curl -X GET -i http://localhost:5000/evento
    pagina = request.args.get('page')
    limite = request.args.get('limit')
    print("pagina", pagina, "limite", limite)

    if pagina != None and limite != None:  # curl -X GET -i 'http://localhost:5000/evento?page=1&limit=1'
        eventos = Evento.query.paginate(int(pagina), int(limite), True)
        result = schema_eventos.dump(eventos.items, many=True)
        return jsonify(result.data)
    else:  # todos los eventos sin paginacion
        eventos = Evento.query.paginate(1, Evento.query.count(), True)
        result = schema_eventos.dump(eventos.items, many=True)
        return jsonify(result.data)


@app.route('/evento/contar', methods=['GET'])
def totalEventos():
    total = Evento.query.count()
    return jsonify(total=total)


'''
curl -X POST -H 'Content-Type: application/json' -i http://localhost:5000/evento --data '{
"nombre": "Prueba"
}'
'''


@app.route('/evento', methods=["POST"])
def crearEvento():
    try:
        nombre = request.json['nombre']
        if nombre != "":
            evento = Evento(nombre=nombre)
            db.session.add(evento)
            db.session.commit()
            return jsonify(schema_evento.dump(evento))
        else:
            return make_response(jsonify(error="Datos incorrectos."), 404)
    except KeyError:
        return make_response(jsonify(error="Los parametros de la solicitud son incorrectos."), 404)


@app.route('/evento/<int:id>', methods=['DELETE'])
def eliminarEvento(id):
    # curl -X DELETE -i http://localhost:5000/evento/1
    try:
        evento = Evento.query.get(id)
        if evento != None:
            db.session.delete(evento)
            db.session.commit()
            return jsonify(message="Evento " + str(id) + " eliminado correctamente.")
        else:
            return make_response(jsonify(error="El evento no existe."), 404)
    except IntegrityError:
        return make_response(jsonify(error="El evento no se puede eliminar porque tiene lecturas asociadas."), 404)


'''
curl -X PUT -H 'Content-Type: application/json' -i http://localhost:5000/evento/1 --data '{
"nombre": "Degesa"
}'
'''


@app.route('/evento/<int:id>', methods=['PUT'])
def editarEvento(id):
    evento = Evento.query.get(id)
    if evento != None:
        evento.nombre = request.json['nombre']
        db.session.add(evento)
        db.session.commit()
        return jsonify(schema_evento.dump(evento))
    else:
        return make_response(jsonify(error="El evento no existe."), 404)
