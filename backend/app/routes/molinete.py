from app import app, db
from flask import render_template, jsonify, make_response, request
from app.models.model import Molinete, schema_molinete, schema_molinetes
from sqlalchemy.exc import IntegrityError

@app.route('/molinete/<int:id>', methods=["GET"])
def getMolinete(id):
    # curl -X GET -i http://localhost:5000/molinete/1
    molinete = Molinete.query.get(id)
    if molinete != None:
        result = schema_molinete.dump(molinete)
        return jsonify(result.data)
    else:
        return make_response(jsonify(error="No existe el molinete."), 404)


@app.route('/molinete', methods=["GET"])
def getMolinetes():
    # curl -X GET -i 'http://localhost:5000/molinete?page=1&limit=1'
    pagina = request.args.get('page')
    limite = request.args.get('limit')
    print("pagina", pagina, "limite", limite)
    if pagina != None and limite != None:
        molinetes = Molinete.query.paginate(int(pagina), int(limite), True)
        result = schema_molinetes.dump(molinetes.items, many=True)
        return jsonify(result.data)
    else: #curl -X GET -i 'http://localhost:5000/molinete'
        molinetes = Molinete.query.paginate(1, Molinete.query.count(), True)
        result = schema_molinetes.dump(molinetes.items, many=True)
        return jsonify(result.data)


@app.route('/molinete/contar', methods=['GET'])
def contarMolinetes():
    cantidad = Molinete.query.count()
    return jsonify(total = cantidad)

'''
curl -X POST -i http://localhost:5000/molinete
'''
@app.route('/molinete', methods=['POST'])
def crearMolinete():
    molinete = Molinete()
    db.session.add(molinete)
    db.session.commit()
    return jsonify(schema_molinete.dump(molinete))


@app.route('/molinete/<int:id>', methods=['DELETE'])
def eliminarMolinete(id):
    # curl -X DELETE -i http://localhost:5000/molinete/1
    try:
        molinete = Molinete.query.get(id)
        if molinete != None:
            db.session.delete(molinete)
            db.session.commit()
            return jsonify(message="Molinete eliminado correctamente")
        else:
            return make_response(jsonify(error="El molinete no existe."), 404)
    except IntegrityError:
        return make_response(jsonify(error="El molinete no se puede eliminar porque tiene lecturas asociadas."), 404)
