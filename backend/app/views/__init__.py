from app.models.model import Credencial, Evento, Lectura, Molinete
from app import db, app
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView

admin = Admin(app)
admin.add_view(ModelView(Evento, db.session))
admin.add_view(ModelView(Molinete, db.session))
admin.add_view(ModelView(Credencial, db.session))
admin.add_view(ModelView(Lectura, db.session))
