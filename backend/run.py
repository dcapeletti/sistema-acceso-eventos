from app import db, app
from sqlalchemy.exc import InternalError, OperationalError

if __name__ == '__main__':
    try:
        db.create_all()
        app.run()
    except InternalError:
        print("\nDebes crear una base de datos en mysql. Revice las configuraciones en el archivo config.py\n")
    except OperationalError:
        print("\nDebes crear una base de datos en mysql. Revice las configuraciones en el archivo config.py\n")
