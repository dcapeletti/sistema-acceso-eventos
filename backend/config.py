'''
Se debera crear una base de datos llamada sistemaacceso con usuario y contrasena igual
'''
class Config(object):
    #Configuraciones x defecto
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://sistemaacceso:sistemaacceso@localhost/sistemaacceso'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = False


class ProductionConfig(Config):
    #Configuraciones para produccion
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://sistemaacceso:sistemaacceso@localhost/sistemaacceso'
    DEBUG = False
    SECRET_KEY = "jdhgdkghfj" #deberia ser una variable del sistema, por ahora lo dejo asi...


class DevelopmentConfig(Config):
    #Configuraciones para desarrollo
    DEBUG = True
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://sistemaacceso:sistemaacceso@localhost/sistemaacceso'
    SQLALCHEMY_ECHO = True
    SECRET_KEY = "jdhgdkghfj"
