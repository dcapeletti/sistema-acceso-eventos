# SISTEMA DE ACCESO PARA EVENTOS

Frontend Vuejs (directorio **frontend**). Backend python Flask (directorio **backend**).
El backend sirve al frontend realizado en vuejs. En ningún momento se usa jinja.
Por tanto, si bien ambas carpetas están en el mismo proyecto,
ambas tecnologías están bien separadas y la dinámica de comunicación es mediante llamadas a la api rest.


# REQUERIMIENTOS NECESARIOS

Tener python3 (3.5 o superior) instalado, pip3, virtualenv, nodejs 10.15 o superior,
vue CLI 3.3 o superior.


# GUIA DE INSTALACIÓN

Los siguientes comandos son necesarios para crear el entorno virtual de desarrollo para backend
e instalar las dependencias necesarias.

`pip3 install virtualenv`

`virtualenv -p python3 venv`

`source venv/bin/activate`

`git clone https://gitlab.com/dcapeletti/sistema-acceso-eventos.git`

`cd sistema-acceso-eventos/backend`

`pip install -r requirements.txt`


# EJECUCIÓN DEL PROYECTO PARA DESARROLLO (dev)

Para ejecutar el proyecto en modo desarrollo (DevelopmentConfig), debe estar en ejecución el servidor frontend. El servidor backend, 
cuando está en este modo, hace un proxy al servidor frontend (http://localhost:8080) y muestra los cambios que se hacen en vuejs.

Primero debe arrancar el servidor backend con:
`python run.py`
Si obtiene un error de que no existe la base de datos, revice el archivo **backend/config.py** para configurar la base de datos, 
usuario y contraseña. Se usa mysql (mariadb), pero puedes configurar el url para postgresql por ejemplo.
Ahora ejecute nuevamente `python run.py` y deberían inicializar la BD con todos los modelos.

Ahora puede arrancar el servidor frontend. Continue leyendo.

## Ejecución del frontend para desarrollo

Mientras está en modo de desarrollo, puede visualizar los cambios del frontend sin necesidad de recargar el navegador.
Para ejecutar el servidor frontend por primera vez, deberá instalar las dependencias de npm.

`cd frontend`

`npm install`

`npm run dev`

Ahora debería poder acceder tanto desde:
[http://localhost:5000](http://localhost:5000)
o desde:
[http://localhost:8080](http://localhost:8080)

Si es así, es porque flask (servidor backend) está en modo desarrollo (DevelopmentConfig). También debería poder acceder al **admin** de flask en
la url [http://localhost:5000/admin/](http://localhost:5000/admin/).

Cuando está en modo producción (ProductionConfig), el proxy no funcionará por lo que únicamente podrá ingresar a 
[http://localhost:5000](http://localhost:5000)


## Compilación del frontend para producción

Si no tiene instaladas las dependencias de npm, debe ubicarse en el directorio frontend y ejecutar el siguiente comando:

`cd frontend`

`npm install`

`npm run build`

Este comando enviará todos los cambios a **backend/dist** con lo cual flask en producción toma inmediatamente dichos cambios.
